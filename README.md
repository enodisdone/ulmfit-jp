# Instructions

## Overview

This package is for reproducing the ULMFiT based results of our paper - [An Investigation of Transfer Learning-Based Sentiment Analysis in Japanese](https://arxiv.org/abs/1905.09642) 

It is based on Fastai research library, built on PyTorch, for developing state-of-the-art deep learning models on a wide variety of NLP/CV tasks.

### Details
As of early November 2018, fast.ai library version 1.0 was under heavy development, I didn't succeed to train. Therefore I've switched to old fastai version 0.7. Please follow the installation instruction. We will soon upgrade to fastai 1.x version.

## Installation
First create conda virtual environment
```
conda create -n ulmfit python=3.6
source activate ulmfit 
pip install .
conda install nb_conda
conda install pytorch=0.4.1 cuda90 -c pytorch
# Used for jupyter lab/notebook
python -m pip install ipykernel
python -m ipykernel install --user
```
To install mecab and mecab-ipadic dictionary, refer to the following links
 - [mecab](http://taku910.github.io/mecab/)
 - [mecab-ipadic](https://github.com/neologd/mecab-ipadic-neologd/blob/master/README.ja.md)
 
Once finished installing, run `pip install -r requirements.txt`

   
## To fine tune LM and build a classifier with yahoo dataset(evaluation included)
 - `yahoo-finetune-classification.ipynb` for training [yahoo movie review dataset](https://github.com/dennybritz/sentiment-analysis/tree/master/data)

Detailed step by step instructions provided.

## Citation
For now please cite our paper on arxiv
```
@ARTICLE{2019arXiv190509642B,
       author = {{Bataa}, Enkhbold and {Wu}, Joshua},
        title = "{An Investigation of Transfer Learning-Based Sentiment Analysis in Japanese}",
      journal = {arXiv e-prints},
     keywords = {Computer Science - Computation and Language, Computer Science - Machine Learning},
         year = "2019",
        month = "May",
          eid = {arXiv:1905.09642},
        pages = {arXiv:1905.09642},
archivePrefix = {arXiv},
       eprint = {1905.09642},
 primaryClass = {cs.CL},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2019arXiv190509642B},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

## TODO
- [ ] Upload pre-trained language model and data files on s3
- [ ] Convert notebook file to python modules(train, finetune, fine-tune classifier etc)
- [ ] Move codebase to fastai v1.x